import '@/bootstrap';
import '@/plugins';
import '@/components';
import '@/layouts';

import Vue from 'vue';
import App from '@/App';
import store from '@/store';
import router from '@/router';
import vuetify from './plugins/vuetify';
import "@/assets/scss/app.scss";

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
