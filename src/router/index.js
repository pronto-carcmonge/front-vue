import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store';
import routes from './routes';

Vue.use(VueRouter);

const scrollBehavior = (to, _from, savedPosition) => {
  if (savedPosition) {
    return savedPosition;
  }

  const position = {};
  if (to.hash) {
    position.selector = to.hash;

    if (to.hash === '#anchor2') {
      position.offset = { y: 100 };
    }

    if (document.querySelector(to.hash)) {
      return position;
    }

    return false;
  }

  return new Promise((resolve) => {
    if (to.matched.some(m => m.meta.scrollToTop)) {


      position.x = 0;
      position.y = 0;
    }

    // this.app.$root.$once('triggerScroll', () => {


    //   resolve(position);
    // });
  });
};

const router = new VueRouter({
  mode: 'history',
  scrollBehavior,
  routes,
});

router.beforeEach((to, from, next) => {
  const isPublic = to.matched.some(record => record.meta.public);
  const onlyWhenLoggedOut = to.matched.some(record => record.meta.onlyWhenLoggedOut);
  const loggedIn = store.getters['auth/token'];

  if (!isPublic && !onlyWhenLoggedOut && !loggedIn) {
    return next({
      name: 'login',
      query: { redirect: to.fullPath },
    });
  }

  if (loggedIn && onlyWhenLoggedOut) {
    return next({ name: 'home' });
  }

  return next();
});

export default router;
