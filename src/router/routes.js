import mapRoutes from './utils';

export default [
  /*
  |--------------------------------------------------------------------------
  | Public Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register routes for public access. These
  | routes do not require authentication and are used by the entire
  | application.
  |
  */

  /*
  |--------------------------------------------------------------------------
  | Guest Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register routes for guest access. These
  | routes require not being authenticated and are public
  | access.
  |
  */

  ...mapRoutes({ layout: 'login', onlyWhenLoggedOut: true }, [
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: 'login' */ '@/views/Login')
    },
    //   {
    //     path: '/register',
    //     name: 'register',
    //     component: () => import(/* webpackChunkName: 'register' */ '@/views/Register'),
    //   },
    //   {
    //     path: '/password/email/',
    //     name: 'password.reset',
    //     component: () => import(/* webpackChunkName: 'reset-password' */ '@/views/PasswordEmail'),
    //   },
    //   {
    //     path: '/password/reset:token',
    //     name: 'password.reset',
    //     component: () => import(/* webpackChunkName: 'reset-password' */ '@/views/PasswordReset'),
    //   },
  ]),

  /*
  |--------------------------------------------------------------------------
  | Protected Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register routes for private access. These
  | routes require authentication and are used for private
  | access.
  |
  */

  ...mapRoutes({ layout: 'default', onlyWhenLoggedOut: false }, [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/Country')
    },
  ]),
  ...mapRoutes({ layout: 'default', onlyWhenLoggedOut: false }, [
    {
      path: '/country',
      name: 'country',
      component: () => import('@/views/Country')
    },
  ]),
  ...mapRoutes({ layout: 'default', onlyWhenLoggedOut: false }, [
    {
      path: '/client',
      name: 'client',
      component: () => import('@/views/Client')
    },
  ]),
  ...mapRoutes({ layout: 'default', onlyWhenLoggedOut: false }, [
    {
      path: '/send-message',
      name: 'sendmessage',
      component: () => import('@/views/SmsSend')
    },
  ]),
  ...mapRoutes({ layout: 'default', onlyWhenLoggedOut: false }, [
    {
      path: '/balance',
      name: 'balance',
      component: () => import('@/views/TwilioBalance')
    },
  ]),
  ...mapRoutes({ layout: 'default', onlyWhenLoggedOut: false }, [
    {
      path: '/segment',
      name: 'segment',
      component: () => import('@/views/Segment')
    },
  ]),
  ...mapRoutes({ layout: 'default', onlyWhenLoggedOut: false }, [
    {
      path: '/send-message-segment',
      name: 'sendmessagesegment',
      component: () => import('@/views/SmsSegmentSend')
    },
  ]),

  /*
  |--------------------------------------------------------------------------
  | Error Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register routes for error messages. These
  | routes are used when there is no match.
  |
  */

  // ...mapRoutes({ layout: 'default', public: true }, [
  //   {
  //     path: '*',
  //     name: '404',
  //     component: () =>
  //       import(/* webpackChunkName: '404' */ '@/views/errors/404')
  //   }
  // ])
];
