export const FETCH_USER_SUCCESS = 'Fetch User Success';
export const FETCH_USER_FAILURE = 'Fetch User Failure';
export const SAVE_TOKEN = 'Save Token';
export const TOGGLE_LOADING = 'Toggle Loading';
export const LOGOUT = 'Logout';

export const FETCH_COUNTRIES_SUCCESS = 'Fetch Countries Success';
export const FETCH_COUNTRIES_FAILURE = 'Fetch Countries Failure';
export const CREATE_COUNTRY = 'Create Country Success';
export const CREATE_COUNTRY_FAILURE = 'Create Country Failure';
export const UPDATE_COUNTRY = 'Update Country Success';
export const UPDATE_COUNTRY_FAILURE = 'Update Country Failure';
export const DELETE_COUNTRY = 'Delete Country Success';
export const DELETE_COUNTRY_FAILURE = 'Delete Country Failure';

export const FETCH_CLIENTS_SUCCESS = 'Fetch Clients Success';
export const FETCH_CLIENTS_FAILURE = 'Fetch Clients Failure';
export const CREATE_CLIENT = 'Create Client Success';
export const CREATE_CLIENT_FAILURE = 'Create Client Failure';
export const UPDATE_CLIENT = 'Update Client Success';
export const UPDATE_CLIENT_FAILURE = 'Update Client Failure';
export const DELETE_CLIENT = 'Delete Client Success';
export const DELETE_CLIENT_FAILURE = 'Delete Client Failure';

export const FETCH_SEGMENTS_SUCCESS = 'Fetch SEGMENT Success';
export const FETCH_SEGMENTS_FAILURE = 'Fetch SEGMENT Failure';
export const CREATE_SEGMENT = 'Create SEGMENT Success';
export const CREATE_SEGMENT_FAILURE = 'Create SEGMENT Failure';
export const UPDATE_SEGMENT = 'Update SEGMENT Success';
export const UPDATE_SEGMENT_FAILURE = 'Update SEGMENT Failure';
export const DELETE_SEGMENT = 'Delete SEGMENT Success';
export const DELETE_SEGMENT_FAILURE = 'Delete SEGMENT Failure';

export const SEND_SMS = 'Send SMS Success';
export const GET_BALANCE = 'Get Balance Success';