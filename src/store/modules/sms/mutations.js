/* eslint-disable no-param-reassign */
import * as types from "@/store/types";

export default {
  [types.SEND_SMS](state, { sms }) {
    state.sms = sms;
  },
  [types.GET_BALANCE](state, data) {
    state.balance = data;
  },
  [types.TOGGLE_LOADING](state) {
    state.isLoading = !state.isLoading;
  },
};
