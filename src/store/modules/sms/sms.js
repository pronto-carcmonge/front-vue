import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const state = {
  sms: null,
  balance: {},
  isLoading: false,
};

export default {
  state,
  getters,
  actions,
  mutations
};