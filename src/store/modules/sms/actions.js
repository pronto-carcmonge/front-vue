import * as types from "@/store/types";
import axios from "axios";

export default {

  async sendSMS({ commit }, params) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.post('/api/sendmessage', params);
      commit(types.SEND_SMS, data);
      return data;
    } catch (e) {
      //commit(types.CREATE_CLIENT_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

  async sendSegmentSMS({ commit }, params) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.post('/api/sendsegmentmessage', params);
      commit(types.SEND_SMS, data);
      return data;
    } catch (e) {
      //commit(types.CREATE_CLIENT_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

  async getBalance({ commit }) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data } = await axios.get('/api/balance');
      commit(types.GET_BALANCE, data);
      return data;
    } catch (e) {
      //commit(types.CREATE_CLIENT_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },
};
