import * as types from "@/store/types";
import axios from "axios";

export default {

  async getSegments({ commit }, params) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.get("/api/segment", params);
      const segments = data.map((segment) => {
        let clients = '';
        segment.clients.forEach(function (element) {
          clients = clients + ` ${element.name},`;
        });
        return {
          id: segment.id,
          name: segment.name,
          clients: clients,
          clients_id: segment.clients,
        };
      });
      commit(types.FETCH_SEGMENTS_SUCCESS, { segments });
      return data;
    } catch (e) {
      //commit(types.FETCH_SEGMENTS_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

  async storeSegment({ commit }, segment) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.post('/api/segment', segment);
      commit(types.CREATE_SEGMENT, data);
      return data;
    } catch (e) {
      //commit(types.CREATE_SEGMENT_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

  async updateSegment({ commit }, segment) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.put(`/api/segment/${segment.id}`, segment);
      commit(types.UPDATE_SEGMENT, data);
      return data;
    } catch (e) {
      //commit(types.UPDATE_SEGMENT_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

  async deleteSegment({ commit }, segment) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.delete(`/api/segment/${segment.id}`, segment);
      commit(types.DELETE_SEGMENT, data);
      return data;
    } catch (e) {
      //commit(types.DELETE_SEGMENT_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

};
