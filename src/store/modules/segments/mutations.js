/* eslint-disable no-param-reassign */
import * as types from "@/store/types";

export default {
  [types.FETCH_SEGMENTS_SUCCESS](state, { segments }) {
    state.segments = segments;
  },
  [types.FETCH_SEGMENTS_FAILURE](state, { segment }) {
    state.segment = segment;
  },
  [types.CREATE_SEGMENT](state, { segment }) {
    state.segment = segment;
  },
  [types.UPDATE_SEGMENT](state, { segment }) {
    state.segment = segment;
  },
  [types.DELETE_SEGMENT](state, { segment }) {
    state.segment = segment;
  },
  [types.TOGGLE_LOADING](state) {
    state.isLoading = !state.isLoading;
  },
};
