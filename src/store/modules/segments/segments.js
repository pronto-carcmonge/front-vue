import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const state = {
  segments: [],
  segment: null,
  isLoading: false,
};

export default {
  state,
  getters,
  actions,
  mutations
};