/* eslint-disable no-param-reassign */
import * as types from "@/store/types";

export default {
  [types.FETCH_COUNTRIES_SUCCESS](state, { countries }) {
    state.countries = countries;
  },
  [types.FETCH_COUNTRIES_FAILURE](state, { country }) {
    state.country = country;
  },
  [types.CREATE_COUNTRY](state, { country }) {
    state.country = country;
  },
  [types.UPDATE_COUNTRY](state, { country }) {
    state.country = country;
  },
  [types.DELETE_COUNTRY](state, { country }) {
    state.country = country;
  },
  [types.TOGGLE_LOADING](state) {
    state.isLoading = !state.isLoading;
  },
};
