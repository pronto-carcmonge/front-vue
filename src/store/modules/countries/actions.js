import * as types from "@/store/types";
import axios from "axios";

export default {

  async getCountries({ commit }, params) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.get("/api/country", params);
      commit(types.FETCH_COUNTRIES_SUCCESS, { countries: data });
      return data;
    } catch (e) {
      //commit(types.FETCH_COUNTRIES_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

  async storeCountry({ commit }, country) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.post('/api/country', country);
      commit(types.CREATE_COUNTRY, data);
      return data;
    } catch (e) {
      //commit(types.CREATE_COUNTRY_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

  async updateCountry({ commit }, country) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.put(`/api/country/${country.id}`, country);
      commit(types.UPDATE_COUNTRY, data);
      return data;
    } catch (e) {
      //commit(types.UPDATE_COUNTRY_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

  async deleteCountry({ commit }, country) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.delete(`/api/country/${country.id}`, country);
      commit(types.DELETE_COUNTRY, data);
      return data;
    } catch (e) {
      //commit(types.DELETE_COUNTRY_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

};
