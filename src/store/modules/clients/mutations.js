/* eslint-disable no-param-reassign */
import * as types from "@/store/types";

export default {
  [types.FETCH_CLIENTS_SUCCESS](state, { clients }) {
    state.clients = clients;
  },
  [types.FETCH_CLIENTS_FAILURE](state, { client }) {
    state.clients = client;
  },
  [types.CREATE_CLIENT](state, { client }) {
    state.client = client;
  },
  [types.UPDATE_CLIENT](state, { client }) {
    state.client = client;
  },
  [types.DELETE_CLIENT](state, { client }) {
    state.client = client;
  },
  [types.TOGGLE_LOADING](state) {
    state.isLoading = !state.isLoading;
  },
};
