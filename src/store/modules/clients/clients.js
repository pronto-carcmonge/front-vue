import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const state = {
  clients: [],
  client: null,
  isLoading: false,
};

export default {
  state,
  getters,
  actions,
  mutations
};