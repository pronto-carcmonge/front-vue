import * as types from "@/store/types";
import axios from "axios";

export default {

  async getClients({ commit }, params) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.get("/api/client", params);
      commit(types.FETCH_CLIENTS_SUCCESS, { clients: data });
      return data;
    } catch (e) {
      //commit(types.FETCH_CLIENTS_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

  async storeClient({ commit }, client) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.post('/api/client', client);
      commit(types.CREATE_CLIENT, data);
      return data;
    } catch (e) {
      //commit(types.CREATE_CLIENT_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

  async updateClient({ commit }, client) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.put(`/api/client/${client.id}`, client);
      commit(types.UPDATE_CLIENT, data);
      return data;
    } catch (e) {
      //commit(types.UPDATE_CLIENT_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

  async deleteClient({ commit }, client) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data: { data } } = await axios.delete(`/api/client/${client.id}`, client);
      commit(types.DELETE_CLIENT, data);
      return data;
    } catch (e) {
      //commit(types.DELETE_CLIENT_FAILURE);
      return null;
    }
    finally {
      commit(types.TOGGLE_LOADING);
    }
  },

};
