/* eslint-disable no-param-reassign */
import * as types from '@/store/types';
import Cookies from 'js-cookie';

export default {

  [types.FETCH_USER_SUCCESS](state, { user }) {
    state.user = user.data;
  },

  [types.FETCH_USER_FAILURE](state) {
    state.token = null;
    Cookies.remove('token');
  },

  [types.SAVE_TOKEN](state, { token, remember }) {
    state.token = token;
    Cookies.set('token', token, { expires: remember ? 365 : null });
  },

  [types.LOGOUT](state) {
    state.user = null;
    state.token = null;
    state.meetings = null;

    Cookies.remove('token');
  },

  [types.TOGGLE_LOADING](state) {
    state.isLoading = !state.isLoading;
  },
};
