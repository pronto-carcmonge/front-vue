/* eslint-disable no-unused-vars */
import axios from 'axios';
import * as types from '@/store/types';

export default {
  saveToken({ commit }, token) {
    commit(types.SAVE_TOKEN, token);
  },

  async fetchUser({ commit }) {
    try {
      const { data } = await axios.get('/api/auth/me');
      commit(types.FETCH_USER_SUCCESS, { user: data });

      return data;
    } catch (e) {
      commit(types.FETCH_USER_FAILURE);
      return null;
    }
  },

  async login({ commit, dispatch }, credentials) {
    try {
      commit(types.TOGGLE_LOADING);

      const { data } = await axios.post('/api/auth/login', credentials);

      await dispatch('saveToken', {
        token: data.token,
        remember: credentials.remember,
      });

      await dispatch('fetchUser');

      return true;
    } catch (e) {
      const { error } = e.response.data;

      return false;
    } finally {
      commit(types.TOGGLE_LOADING);
    }
  },

  async forgot({ dispatch }, email) {
    try {
      await axios.post('/api/auth/password/email', email);

      return true;
    } catch (e) {
      throw e;
    }
  },

  async reset({ dispatch }, credentials) {
    try {
      await axios.post('/api/auth/password/reset', credentials);

      return true;
    } catch (e) {
      throw e;
    }
  },

  async logout({ commit }) {
    try {
      await axios.post('/api/auth/logout');
    } catch (e) {
      throw e;
    } finally {
      commit(types.LOGOUT);
    }
  },
};
