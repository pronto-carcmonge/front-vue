import Vue from 'vue';
import axios from 'axios';
import store from '@/store';

axios.defaults.baseURL = process.env.VUE_APP_API_URL;

axios.interceptors.request.use(async request => {
  const token = store.getters["auth/token"];

  if (token) {
    request.headers.common.Authorization = `Bearer ${token}`;
  }

  return request;
});

Vue.prototype.axios = axios;
Vue.prototype.$http = axios;

window.axios = axios;
