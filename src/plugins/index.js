const requirePlugin = require.context(
    ".",
    false,
    /\.js$/
);

requirePlugin.keys().forEach(requirePlugin);
