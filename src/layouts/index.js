import Vue from 'vue';
import upperFirst from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';

const requireLayouts = require.context(
    '.',
    false,
    /[a-z]\w+\.(vue)$/,
    'lazy',
);

requireLayouts.keys().forEach((fileName) => {
    const layoutName = upperFirst(
        camelCase(
            `${fileName.replace(/^\.\/(.*)\.\w+$/, '$1')}Layout`,
        ),
    );

    Vue.component(
        layoutName,
        () => requireLayouts(fileName),
    );
});
